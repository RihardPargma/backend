<?php

namespace App\Http\Controllers;

use App\Models\Products;
use Illuminate\Http\Request;

class StoreController extends Controller

{
    public function index()
    {
        return view('welcome' , [
            'products' => Products::all(),
            'cart' => session('cart', [])
            ]);
    }
    public function addtocart(Request $request)
    {
        $cart = $request->session()->get('cart');

        $quantity = 0;
        if($request->qty <= 0){
            $quantity = 1;
        }else{
            $quantity = $request->quantity;
        }

        $product = Products::find($request->id);
        $productitem = [
            'id' => $product->id,
            'name' => $product->name,
            'description' => $product->description,
            'price' => $product->price,
            'image' => $product->image,
            'quantity' => $quantity
        ];
        if (!$cart) {
            $cart = [
                $product->id => $productitem
            ];
            $request->session()->put('cart', $cart);
            return redirect()->back();
        }
        if (isset($cart[$product->id])) {
            $cart[$product->id]['quantity']++;
            $request->session()->put('cart', $cart);
            return redirect()->back();
        }
        $cart[$product->id] = $productitem;
        $request->session()->put('cart', $cart);
        return redirect()->back();
    }

    public function cart(){

        return view('cart', [
            'cart' => session('cart', [])
        ]);
    }
    public function updatecart(Request $request){
        $quantity = 0;
        if($request->quantity <= 0){
            $quantity = 1;
        }else{
            $quantity = $request->quantity;
        }
        $cart = session('cart');
        $cart[$request->id]['quantity'] = $quantity;
        $request->session()->put('cart', $cart);
        return redirect()->back();
    }
    public function deleteitem(Request $request){
        $cart = session('cart');
        unset($cart[$request->id]);
        $request->session()->put('cart', $cart);
        return redirect()->back();
    }
}
