<?php

namespace App\Http\Controllers;

use App\models\User;
use Illuminate\Http\Request;

class DashboardController extends Controller
{
    public function index()
    {
        return view('dashboard', [
            'users' => User::all()
        ]);
    }
}
